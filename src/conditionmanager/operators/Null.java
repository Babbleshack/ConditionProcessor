/**
 * the null operator, handles situations where there is no operation
 * @author Dominic Lindsay
 */
package conditionmanager.operators;

public class Null implements Operator {
    public Null(){}
    @Override
    public boolean operate(boolean r1, boolean r2) {
        return r1;
    }

}
