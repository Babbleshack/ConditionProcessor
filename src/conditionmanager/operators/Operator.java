/*
 * Defines a 'Command' style operator class.
 */
package conditionmanager.operators;

/**
 *
 * @author babbleshack
 */
public interface Operator {
    public boolean operate(boolean r1, boolean r2);
}
