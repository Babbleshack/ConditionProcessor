/**
 * condition producer
 * @author Dominic Lindsay
 */
package conditionmanager.producers;

import conditionmanager.conditionContainers.ActuatorCondition;
import conditionmanager.database.IQueryManager;
import java.util.HashSet;
import java.util.concurrent.BlockingQueue;


public class ConditionProducer implements Runnable {
    private BlockingQueue _sharedQueue;
    private final IQueryManager _qm;
    public ConditionProducer(BlockingQueue sharedQueue, 
            final IQueryManager quearyManager) {
        this._sharedQueue = sharedQueue;
        this._qm = quearyManager;
    }
    /**
     * builds a set of actuator ids, itterates set add condition for each
     * id to shared queue
     */
    public void run() {
        HashSet<Integer> actuatorIds = new HashSet<Integer>();
        ActuatorCondition ac;
        while(true) {
            actuatorIds.addAll(_qm.getActuatorIds());
            for(Integer i : actuatorIds){
                System.out.println("Got ID: " + i);
            }
            for(Integer i : actuatorIds){
                try {
                    
                    if((ac = _qm.getFirstCondition(i.intValue())) == null)
                        continue;
                    _sharedQueue.put(ac);
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    System.err.println(ConditionProducer.class.getName() + "Failed");
                }
            }
        }
    }
    
}
