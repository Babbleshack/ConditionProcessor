/*
 * interface for QuearyManager
 * @author Dominic Lindsay
 */
package conditionmanager.database;

import conditionmanager.conditionContainers.ActuatorCondition;
import java.util.ArrayList;


public interface IQueryManager {

    /**
     * Check if a given actuator job id meets its job thresholds
     * @param  actuator_job_id  id of actuator job to check
     * @return boolean          true or false
     */
    boolean checkActuatorJob(int actuator_job_id);

    /* returns a list of ActuatorIds() */
    ArrayList<Integer> getActuatorIds();

    /**
     * Get the first condition for a given actuator
     * @param  actuator_id actuator id
     * @return             ActuatorCondition - first condition for a given actuator
     */
    ActuatorCondition getFirstCondition(int actuator_id);

    ActuatorCondition getNextCondition(int condition_id);

    /**
     * Returns if actuator is currently on or not
     * @param  actuator_id  Actuator to check
     * @return       int    1 if actuator is on, 0 if actuator is off
     */
    int isActuatorOn(int actuator_id);

    boolean isActuatorWithinAutoTime(int actuator_id);

    /**
     * Checks if actuator is on, if it is set it to off
     * @param actuator_id actuator id
     */
    void setRelayOff(int actuator_id);

    /**
     * Checks if actuator is on, if it isn't set it to on
     * @param actuator_id actuator id
     */
    void setRelayOn(int actuator_id);

    /**
     *returns bool if actuator is in auto mode
     */
    boolean isAutoMode(int actId);
    
}
