/**
 *
 * @author Dominic Lindsay
 */
package conditionmanager.database;

import java.util.ArrayList;
import conditionmanager.conditionContainers.ActuatorCondition;
import conditionmanager.operators.And;
import conditionmanager.operators.Null;
import conditionmanager.operators.Operator;
import conditionmanager.operators.Or;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.sql.Date;


public class QueryManager implements IQueryManager {
    IDatabaseConnectionManager connection;
    public QueryManager()
    {
        connection = DatabaseConnectionFactory.createMySQLConnection();
    }
   /**
     * Check if a given actuator job id meets its job thresholds
     * @param  actuator_job_id  id of actuator job to check
     * @return boolean          true or false                 
     */
    @Override
    public boolean checkActuatorJob(int actuator_job_id)
    {
        if(actuator_job_id == 0) return true; 
        
        boolean status = false; 

        int job_id = this.getJobIdFromActuatorJobId(actuator_job_id); 
        // If job id found
        if(job_id >= 0) {
            double latest_reading = this.getLatestReadingFromJobId(job_id); 
            if(latest_reading >= 0) {
                String direction = this.getDirectionFromActuatorJobId(actuator_job_id); 
                double threshold = this.getThresholdFromActuatorJobId(actuator_job_id); 

                if(direction.equals("ABOVE")) {
                    System.out.println("Job ID: " + job_id + ": " + latest_reading + " > " + threshold + "?: " + (latest_reading > threshold));
                    if(latest_reading > threshold) status = true; 
                } else if(direction.equals("BELOW")) {
                    System.out.println("Job ID: " + job_id + ": " + latest_reading + " < " + threshold + "?: " + (latest_reading < threshold));
                    if(latest_reading < threshold) status = true; 
                } else if(direction.equals("EQUALS")) {
                    System.out.println("Job ID: " + job_id + ": " + latest_reading + " = " + threshold + "?: " + (latest_reading == threshold));
                    if(latest_reading == threshold) status = true; 
                }

                // If (difference between time now and reading created at time) <= (actuator job seconds)
                // evaluate to false
                int event_seconds = this.getSecondsFromActuatorJobId(actuator_job_id); 
                long time_now_seconds = this.getTimeInSeconds(); 
                long latest_reading_time_seconds = this.getLatestReadingTimeInSecondsFromJobId(job_id); 
                System.out.println("Diff: " + (time_now_seconds - latest_reading_time_seconds));
                if(event_seconds > 0 && (time_now_seconds - latest_reading_time_seconds) <= event_seconds) 
                    status = false; 
                else 
                    status = (status == true) ? true : false;

            } else {
                System.out.println("Job ID: " + job_id + ": No readings: " + false);
            }

        } else {

            System.out.println("checkActuatorJob() - Could not find job id (" + job_id + ") for actuator id: " + actuator_job_id);
            
        }

        // Set actuator job's current status
        this.setActuatorJobStatus(actuator_job_id, status);

        return status; 
    }

    /**
     * Sets actuator job's status
     * @param actuator_job_id   actuator job to update
     * @param status            status to change to
     */
    private void setActuatorJobStatus(int actuator_job_id, boolean status)
    {
        PreparedStatement update = null;
        if(this.getActuatorJobStatus(actuator_job_id) == status) return; 
        String setRelayOn = "UPDATE `actuator_job` SET `status` = ?, `updated_at` = ? "
                + "WHERE `id` = ?";
        try {
            update = connection.getConnection().prepareStatement(setRelayOn);
            update.setBoolean(1, status); 
            update.setTimestamp(2, new Timestamp(System.currentTimeMillis())); 
            update.setInt(3, actuator_job_id); 
            update.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if(connection.getStatus()) {
                try { if (update != null) update.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }

    /**
     * Returns actuator job status
     * @param  actuator_job_id  actuator job to check
     * @return       boolean true if actuator job active, false if actuator job not active
     */
    private boolean getActuatorJobStatus(int actuator_job_id)
    {
        String getActuatorJobStatus = "SELECT * FROM `actuator_job` WHERE `id` = ?";

        PreparedStatement record = null;
        ResultSet result = null;
        try {
            /**
             * Execute select query
             */
            record =
                connection.getConnection().prepareStatement(getActuatorJobStatus);
            record.setInt(1, actuator_job_id);

            /**
             * Access ResultSet for actuator_address
             */
            result = record.executeQuery();

            /**
             * Return result
             */
            if(result.next()) {
                return result.getBoolean("status");
            } else {
                return false;
            }
        } catch (SQLException e) {
                System.err.println("SQL Exception while preparing/Executing "
                + "getActuatorJobStatus: " + e);
                return false;
        } finally {
            if(connection.getStatus()) {
                try { if (result != null) result.close(); } catch (Exception e) {};
            try { if (record != null) record.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }

    /**
     * Return an operator, given a string for an operator
     * @param  operation string representing an operation
     * @return           Or / And 
     */
    private Operator returnOperator(String operation) 
    {
        if(operation == null){
            return new Null();
        }
        if(operation.equals("or")){ 
            return new Or(); 
        } else if(operation.equals("and")) {
            return new And();
        }
        return new Null();
    }

    /**
     * Get the first condition for a given actuator
     * @param  actuator_id actuator id
     * @return             ActuatorCondition - first condition for a given actuator
     */
    @Override
    public ActuatorCondition getFirstCondition(int actuator_id)
    {
        String getFirstCondition = "SELECT * " 
                + " FROM `Condition` "
                + " WHERE `actuator_id` = ? "
                + " ORDER BY `id` LIMIT 1";
        ActuatorCondition actCondition;

        ResultSet result = null;
        PreparedStatement record = null;
        try {
            /**
             * Execute select query
             */
            record =
                connection.getConnection().prepareStatement(getFirstCondition);
            record.setInt(1, actuator_id);

            /**
             * Access ResultSet for condition
             */
            result = record.executeQuery();
            if(result.next()) {
                /**
                 * Return result
                 */
                actCondition = new ActuatorCondition(
                        result.getInt("id"), 
                        actuator_id, result.getInt("actuator_job"), 
                        result.getInt("second_actuator_job"), 
                        this.returnOperator(result.getString("boolean_operator")), 
                        this.returnOperator(result.getString("next_operator"))
                );
                //System.out.println("CONDITION ID = " + actCondition.getCondID());
                //System.out.println("SECOND ACTUATOR JOB ==: " + actCondition.getActJob2());
                // System.out.println("SECOND QUEARY ==: " + result.getInt("second_actuator_job"));
                return actCondition;
            } else {
                System.out.println("No actuator condition");
                return null;
            }

        } catch (SQLException e) {
                System.err.println("SQL Exception while preparing/Executing "
                + "getFirstCondition: " + e);
                return null;
        } finally {
            if(connection.getStatus()) {
                try { if (result != null) result.close(); } catch (Exception e) {};
            try { if (record != null) record.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }

    /**
     * Private method for accessing a condition directly by its ID
     * @param  condition_id condition id
     * @return              ActuatorCondition for the given id
     */
    private ActuatorCondition getCondition(int condition_id)
    {        
        String getCondition = "SELECT * " 
                + " FROM `Condition`"
                + " WHERE `id` = ? "
                + " ORDER BY `id` LIMIT 1";

        PreparedStatement record = null;
        ResultSet result = null;
        try {
            /**
             * Execute select query
             */
            record =
                connection.getConnection().prepareStatement(getCondition);
            record.setInt(1, condition_id);

            /**
             * Access ResultSet for condition
             */
            result = record.executeQuery();
            if(result.next()) {
                /**
                 * Return result
                 */
                return new ActuatorCondition(result.getInt("id"), result.getInt("actuator_id"), result.getInt("actuator_job"), result.getInt("second_actuator_job"), this.returnOperator(result.getString("boolean_operator")), this.returnOperator(result.getString("next_operator")));
            } else {
                return null;
            }

        } catch (SQLException e) {
                System.err.println("SQL Exception while preparing/Executing "
                + "getCondition: " + e);
                return null;
        } finally {
            if(connection.getStatus()) {
                try { if (result != null) result.close(); } catch (Exception e) {};
            try { if (record != null) record.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }

    @Override
    public ActuatorCondition getNextCondition(int condition_id)
    {
        String getNextCondition = "SELECT * " 
                + " FROM `Condition`"
                + " WHERE `id` = ? "
                + " ORDER BY `id` LIMIT 1";
        PreparedStatement record = null; 
        ResultSet result = null;
        try {
            /**
             * Execute select query
             */
            record =
                connection.getConnection().prepareStatement(getNextCondition);
            record.setInt(1, condition_id);

            /**
             * Access ResultSet for condition
             */
            result = record.executeQuery();
            if(result.next()) {
                /**
                 * Return result
                 */
                return this.getCondition(result.getInt("next_condition")); 
            } else {
                return null;
            }

        } catch (SQLException e) {
                System.err.println("SQL Exception while preparing/Executing "
                + "getNextCondition: " + e);
                return null;
        } finally {
            if(connection.getStatus()) {
                try { if (result != null) result.close(); } catch (Exception e) {};
            try { if (record != null) record.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }

    @Override
    public ArrayList<Integer> getActuatorIds()
    {
        String getActuatorIds = "SELECT * " 
                + " FROM Actuator";
        PreparedStatement record = null;
        ResultSet result = null;
        try {
            /**
             * Execute select query
             */
            record =
                connection.getConnection().prepareStatement(getActuatorIds);

            /**
             * Return result
             */
            ArrayList output_array = new ArrayList<Integer>();
            result = record.executeQuery();
            while (result.next()) {
                output_array.add((Object)Integer.valueOf(result.getInt("id")));
            }

            return output_array;

        } catch (SQLException e) {
                System.err.println("SQL Exception while preparing/Executing "
                + "getActuatorIds: " + e);
                return null;
        } finally {
            if(connection.getStatus()) {
                try { if (result != null) result.close(); } catch (Exception e) {};
            try { if (record != null) record.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }

    /**
     * Checks if actuator is on, if it isn't set it to on
     * @param actuator_id actuator id
     */
    @Override
    public void setRelayOn(int actuator_id){
        if(this.isActuatorOn(actuator_id) == 0) 
            this.setRelayStatus(actuator_id, 1);
    }

    /**
     * Checks if actuator is on, if it is set it to off
     * @param actuator_id actuator id
     */
    @Override
    public void setRelayOff(int actuator_id){
        if(this.isActuatorOn(actuator_id) == 1) 
            this.setRelayStatus(actuator_id, 0);
    }

    /**
     * Sets relay status
     * @param actuator_id   actuator id to change
     * @param status        status to change to
     *                      1 = on
     *                      0 = off
     */
    private void setRelayStatus(int actuator_id, int status)
    {
        String setRelayOn = "UPDATE `Actuator` SET `is_on` = ? "
                + "WHERE `id` = ?";
        PreparedStatement update = null;
        try {
            update = connection.getConnection().prepareStatement(setRelayOn);
            update.setInt(1, status); 
            update.setInt(2, actuator_id); 
            update.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if(connection.getStatus()) {
                try { if (update != null) update.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }

    /**
     * Returns if actuator is currently on or not
     * @param  actuator_id  Actuator to check
     * @return       int    1 if actuator is on, 0 if actuator is off   
     */
    @Override
    public int isActuatorOn(int actuator_id)
    {
        String isActuatorOn = "SELECT * FROM `Actuator` WHERE `id` = ?";

        PreparedStatement record = null;
        ResultSet result = null;
        try {
            /**
             * Execute select query
             */
            record =
                connection.getConnection().prepareStatement(isActuatorOn);
            record.setInt(1, actuator_id);

            /**
             * Access ResultSet for actuator_address
             */
            result = record.executeQuery();

            /**
             * Return result
             */
            if(result.next()) {
                if(result.getInt("is_on") == 1)
                    return 1;
                else
                    return 0;
            } else {
                return 0;
            }
        } catch (SQLException e) {
                System.err.println("SQL Exception while preparing/Executing "
                + "isActuatorOn: " + e);
                return 0;
        } finally {
            if(connection.getStatus()) {
                try { if (result != null) result.close(); } catch (Exception e) {};
            try { if (record != null) record.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }

    @Override
    public boolean isActuatorWithinAutoTime(int actuator_id)
    {
        Timestamp start = this.actuatorStartTime(actuator_id); 
        Timestamp end = this.actuatorEndTime(actuator_id); 
        long now = System.currentTimeMillis(); 
                
        if(start != null && end != null) {
            System.out.println("Start + End time set...");
            if((now > start.getTime()) && (now < end.getTime())) {
                System.out.println("Time now is between Start + End");
                return true; 
            } else {
                System.out.println("Time now ("+now+") is not between Start ("+start.getTime()+") + End ("+end.getTime()+")");
                return false;
            }
        } else {
            // Both start + end not set, just return true
            System.out.println("Start + End time not set...");
            return true; 
        }
    }

    /**
     * Returns auto start time of this actuator
     * @param  actuator_id  Actuator to check
     * @return       int    1 if actuator is on, 0 if actuator is off   
     */
    private Timestamp actuatorStartTime(int actuator_id)
    {
        String actuatorStartTime = "SELECT * FROM `Actuator` WHERE `id` = ?";

        PreparedStatement record = null;
        ResultSet result = null;
        try {
            /**
             * Execute select query
             */
            record =
                connection.getConnection().prepareStatement(actuatorStartTime);
            record.setInt(1, actuator_id);

            /**
             * Access ResultSet for actuator_address
             */
            result = record.executeQuery();

            /**
             * Return result
             */
            if(result.next() && result.getString("auto_start_time") != null) {
                String str = result.getString("auto_start_time");
                Timestamp timestamp =
                    Timestamp.valueOf(
                        new SimpleDateFormat("yyyy-MM-dd ")
                        .format(new Date(System.currentTimeMillis())) // get the current date as String
                        .concat(str)        // and append the time
                    );
                return timestamp;
            } else {
                return null;
            }
        } catch (SQLException e) {
                System.err.println("SQL Exception while preparing/Executing "
                + "actuatorStartTime: " + e);
                return null;
        } finally {
            if(connection.getStatus()) {
                try { if (result != null) result.close(); } catch (Exception e) {};
            try { if (record != null) record.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }

    /**
     * Returns auto end time of this actuator
     * @param  actuator_id  Actuator to check
     * @return       int    1 if actuator is on, 0 if actuator is off   
     */
    private Timestamp actuatorEndTime(int actuator_id)
    {
        String actuatorEndTime = "SELECT * FROM `Actuator` WHERE `id` = ?";

        PreparedStatement record = null;
        ResultSet result = null;
        try {
            /**
             * Execute select query
             */
            record =
                connection.getConnection().prepareStatement(actuatorEndTime);
            record.setInt(1, actuator_id);

            /**
             * Access ResultSet for actuator_address
             */
            result = record.executeQuery();

            /**
             * Return result
             */
            if(result.next() && result.getString("auto_end_time") != null) {
                String str = result.getString("auto_end_time");
                Timestamp timestamp =
                    Timestamp.valueOf(
                        new SimpleDateFormat("yyyy-MM-dd ")
                        .format(new Date(System.currentTimeMillis())) // get the current date as String
                        .concat(str)        // and append the time
                    );
                return timestamp;
            } else {
                return null;
            }
        } catch (SQLException e) {
                System.err.println("SQL Exception while preparing/Executing "
                + "actuatorEndTime: " + e);
                return null;
        } finally {
            if(connection.getStatus()) {
                try { if (result != null) result.close(); } catch (Exception e) {};
            try { if (record != null) record.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }
    
    private long getLatestReadingTimeInSecondsFromJobId(int job_id)
    {
        try {
            SimpleDateFormat df = new SimpleDateFormat(
        "yyyy-MM-dd HH:mm:ss");
            java.util.Date date = df.parse(this.getLatestReadingTimeFromJobId(job_id));
            return (date.getTime()/1000);
        } catch(Exception e){
            System.out.println("Exception while getTimeInSecondsFromActuatorJobId(): " + e);
        }
        return -1;
    }

    private int getSecondsFromActuatorJobId(int actuator_job_id) 
    {
        return Integer.parseInt(this.getColumnFromActuatorJobId(actuator_job_id, "seconds"));
    }

    private int getJobIdFromActuatorJobId(int actuator_job_id) 
    {
        return Integer.parseInt(this.getColumnFromActuatorJobId(actuator_job_id, "job_id"));
    }

    private double getThresholdFromActuatorJobId(int actuator_job_id)
    {
        return Double.parseDouble(this.getColumnFromActuatorJobId(actuator_job_id, "threshold"));
    }

    private String getDirectionFromActuatorJobId(int actuator_job_id)
    {
       return this.getColumnFromActuatorJobId(actuator_job_id, "direction");
    }

    private String getColumnFromActuatorJobId(int actuator_job_id, String column)
    {
        PreparedStatement record = null;
        ResultSet result = null;
        try
        {
            String getColumnFromActuatorJobId = "SELECT * "
                    + " FROM actuator_job"
                    + " WHERE id = ? "
                    + " LIMIT 1";

            /**
             * Execute select query
             */
            record = connection.getConnection().prepareStatement(getColumnFromActuatorJobId);
            record.setInt(1, actuator_job_id);

            /**
             * Access ResultSet for zone_id
             */
            result = record.executeQuery();
            if(result.next())
            {
                /**
                 * Return result
                 */
                return (String)result.getString("actuator_job." + column);
            }
            else
            {
                return null;
            }

        }
        catch (SQLException e)
        {
                System.err.println("SQL Exception while preparing/Executing "
                + "getColumnFromActuatorJobId: " + e);
                return null;
        } finally {
            if(connection.getStatus()) {
                try { if (result != null) result.close(); } catch (Exception e) {};
            try { if (record != null) record.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }

    private long getTimeInSeconds()
    {
        return System.currentTimeMillis() / 1000l;
    }

    /**
     * Get latest reading for a job, given a job id
     * @param  job_id job id
     * @return        [description]
     */
    private double getLatestReadingFromJobId(int job_id)
    {
        String reading_table = this.getReadingTableFromJobId(job_id);
        String reading_field = this.getReadingFieldFromJobId(job_id);
        
        if(reading_table == null || reading_field == null) return -1;

        String getReading = "SELECT * "
                + " FROM " + reading_table
                + " WHERE job_id = ? "
                + " ORDER BY id DESC "
                + " LIMIT 1";

        PreparedStatement record = null;
        ResultSet result = null;
        try {
            /**
             * Execute select query
             */
            record = connection.getConnection().prepareStatement(getReading);
            record.setInt(1, job_id);

            /**
             * Access ResultSet for zone_id
             */
            result = record.executeQuery();
            if(result.next())
            {
                /**
                 * Return result
                 */
                return result.getDouble(reading_table + "." + reading_field);
            }
            else
            {
                return -1;
            }

        } catch (SQLException e)
        {
                System.err.println("SQL Exception while preparing/Executing "
                + "getReading: " + e);
                return -1;
        } finally {
            if(connection.getStatus()) {
                try { if (result != null) result.close(); } catch (Exception e) {};
            try { if (record != null) record.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }

    /**
     * Get latest reading for a job, given a job id
     * @param  job_id job id
     * @return        [description]
     */
    private String getLatestReadingTimeFromJobId(int job_id)
    {
        String reading_table = this.getReadingTableFromJobId(job_id);
        String reading_field = this.getReadingFieldFromJobId(job_id);
        
        if(reading_table == null || reading_field == null) return null;

        String getReading = "SELECT * "
                + " FROM " + reading_table
                + " WHERE job_id = ? "
                + " ORDER BY id DESC "
                + " LIMIT 1";

        PreparedStatement record = null;
        ResultSet result = null;
        try {
            /**
             * Execute select query
             */
            record = connection.getConnection().prepareStatement(getReading);
            record.setInt(1, job_id);

            /**
             * Access ResultSet for zone_id
             */
            result = record.executeQuery();
            if(result.next())
            {
                /**
                 * Return result
                 */
                return result.getString(reading_table + ".created_at");
            }
            else
            {
                return null;
            }

        } catch (SQLException e)
        {
                System.err.println("SQL Exception while preparing/Executing "
                + "getReading: " + e);
                return null;
        } finally {
            if(connection.getStatus()) {
                try { if (result != null) result.close(); } catch (Exception e) {};
            try { if (record != null) record.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }

    private String getReadingTableFromJobId(int job_id) {
        String getReadingTableFromJobId = "SELECT * "
                + " FROM Job, Sensor"
                + " WHERE Sensor.id = Job.sensor_id "
                + " AND Job.id = ? "
                + " LIMIT 1";

        ResultSet result = null;
        PreparedStatement record = null;
        try {
            /**
             * Execute select query
             */
            record = connection.getConnection().prepareStatement(getReadingTableFromJobId);
            record.setInt(1, job_id);

            /**
             * Access ResultSet for zone_id
             */
            result = record.executeQuery();
            if(result.next()) {
                /**
                 * Return result
                 */
                return result.getString("Sensor.table");
            } else {
                return null;
            }

        } catch (SQLException e) {
                System.err.println("SQL Exception while preparing/Executing "
                + "getReadingTableFromJobId: " + e);
                return null;
        } finally {
            if(connection.getStatus()) {
                try { if (result != null) result.close(); } catch (Exception e) {};
                try { if (record != null) record.close(); } catch (Exception e) {};
                try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
            }
        }
    }

    private String getReadingFieldFromJobId(int job_id) {
        String getReadingTableFromJobId = "SELECT * "
                + " FROM Job, Sensor"
                + " WHERE Sensor.id = Job.sensor_id "
                + " AND Job.id = ? "
                + " LIMIT 1";

        PreparedStatement record = null;
        ResultSet result = null;
        try {
            /**
             * Execute select query
             */
            record = connection.getConnection().prepareStatement(getReadingTableFromJobId);
            record.setInt(1, job_id);

            /**
             * Access ResultSet for zone_id
             */
            result = record.executeQuery();
            if(result.next()) {
                /**
                 * Return result
                 */
                return result.getString("Sensor.field");
            } else {
                return null;
            }

        } catch (SQLException e) {
                System.err.println("SQL Exception while preparing/Executing "
                + "getReadingTableFromJobId: " + e);
                return null;
        } finally {
            if(connection.getStatus()) {
                try { if (result != null) result.close(); } catch (Exception e) {};
            try { if (record != null) record.close(); } catch (Exception e) {};
            try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
        }
        }
    }
    public boolean isAutoMode(int actId){
       Connection conn = null;
       PreparedStatement stmt = null;
       ResultSet rs = null;
       try {
            conn = connection.getConnection();
            stmt = conn.prepareStatement("SELECT `is_on` FROM Actuator "
                    + "WHERE `id` = ?");
            stmt.setInt(1, actId);
            rs = stmt.executeQuery();
            if(rs.next()) {
                int test = rs.getInt("is_on");
                System.out.println("Checking ID: \t" + actId);
                if(rs.wasNull()) 
                    return true;
                return false;
            }
            return false;
        } catch (Exception e) {
            System.err.println("Error getting actuator running mode.");
            e.printStackTrace();
        } finally {
            if(connection.getStatus()) {
                try { if (rs != null) rs.close(); } catch (Exception e) {};
                try { if (stmt != null) stmt.close(); } catch (Exception e) {};
                try { if (connection.getConnection() != null) connection.getConnection().close(); } catch (Exception e) {};
            }
        }
       return false;
    }
}
