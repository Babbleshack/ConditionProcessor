/**
 *
 * @author Dominic Lindsay
 */
package conditionmanager.consumers;

import java.util.concurrent.BlockingQueue;
import conditionmanager.conditionContainers.ActuatorCondition;
import conditionmanager.conditionContainers.Result;
import conditionmanager.database.IQueryManager;
import conditionmanager.database.QueryManager;
import conditionmanager.operators.Null;



public class ConditionProcessor implements Runnable {
    private final IQueryManager _qm;
    private Result _res;
    private BlockingQueue _sharedQueue;
    private static final int FAKE_RELAY = -1;
    private static final long PAUSE = 1000/4;
    public ConditionProcessor(BlockingQueue sharedBlockingQueue,
            final IQueryManager queryManager ) {
        this._qm = queryManager;
        this._sharedQueue = sharedBlockingQueue;
    }
    public void run(){
        ActuatorCondition actCond = null;
        _res = null;
        int relayId;
        while(true/*add a stopping flag*/){
            try {
                //take item of queue, blocking if its empty.
                actCond = (ActuatorCondition) _sharedQueue.take();
            } catch (InterruptedException ex) {
                System.err.println("Exception thrown: " + 
                        (ConditionProcessor.class.getName()));
            } finally { /*didnt get an actuator cond, should never happen*/
                if(actCond == null){
                    System.out.println("Skipping");
                    continue;
                }
            }
            //grab relay id
            relayId = actCond.getActID();
            //create a new result object with the result of the 
            //actuator condition
            _res = new Result(
                    actCond.getOp().operate(
                        _qm.checkActuatorJob(actCond.getActJob1()),
                        _qm.checkActuatorJob(actCond.getActJob2())
                    )
            );
            System.out.println("here");
            //go through 'next' conditions.
            while(!(actCond.getNext_op() instanceof Null))
            {
                actCond = _qm.getNextCondition(actCond.getCondID());
                //reaval result with result of new condition 'Operator' 
                //old condition result. i.e. res OR new cond
                _res.setResult(actCond.getNext_op().operate(
                        _res.getResult(), actCond.getOp().operate(
                        _qm.checkActuatorJob(actCond.getActJob1()),
                        _qm.checkActuatorJob(actCond.getActJob2())
                    )));
            }
            //eval last cond ----REPEATED CODE SHOOULD BE REFACTORED-----
            actCond = _qm.getNextCondition(actCond.getCondID());
            
            if(actCond != null) {
                //reaval result with result of new condition 'Operator' 
                //old condition result. i.e. res OR new cond
                _res.setResult(actCond.getNext_op().operate(
                        _res.getResult(), actCond.getOp().operate(
                        _qm.checkActuatorJob(actCond.getActJob1()),
                        _qm.checkActuatorJob(actCond.getActJob2())
                    )));
            }
            
            //take action on sensors
            if(_qm.isActuatorWithinAutoTime(relayId) && _res.getResult())
                _qm.setRelayOn(relayId);
            else 
                _qm.setRelayOff(relayId);
            relayId = FAKE_RELAY;
            try {
                //give DB a quick break
                Thread.sleep(PAUSE);
            } catch (InterruptedException ex) {
                System.err.println("SLEEP ERROR");
            }
        }
    }
}
