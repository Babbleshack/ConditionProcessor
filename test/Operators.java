/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import conditionmanager.operators.And;
import conditionmanager.operators.Null;
import conditionmanager.operators.Or;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author babbleshack
 */
public class Operators {
    
    public Operators() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testAndOperator(){
        And and = new And();
        assertTrue("T and T != T", and.operate(true, true));
        assertFalse("T and F != F", and.operate(true, false));
        assertFalse("F and T != F", and.operate(false, true));
        assertFalse("F and F != F", and.operate(false, false));
    }
    
    @Test
    public void testOrOperator(){
        Or or = new Or();
        assertTrue("T or T != T", or.operate(true, true));
        assertTrue("F or T != T", or.operate(false, true));
        assertFalse("F or F != F", or.operate(false, false));
        assertTrue("T or F != T", or.operate(true, false));
    }
    
    @Test
    public void testNullOperator(){
        Null nullOp = new Null();
        assertTrue("T null T!= T", nullOp.operate(true, true));
        assertFalse("F null T!= F", nullOp.operate(false, true));
        assertFalse("F null F!= F", nullOp.operate(false, false));
        assertTrue("T null F!= T", nullOp.operate(true, false));
    }
}
